#    Friendly Telegram (telegram userbot)
#    Copyright (C) 2018-2019 The Authors

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as publishedby
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.

#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.


import logging
import random
from .. import loader
logger = logging.getLogger(__name__)


@loader.tds
class MyMod(loader.Module):
    """Mods by @AjmalShajahan97"""
    strings = {"name": "My Mods"}

    @loader.owner
    async def upcmd(self, message):
        """Replies with ☝️"""
        await message.edit("☝️")

    @loader.owner
    async def downcmd(self, message):
        """Replies with 👇"""
        await message.edit("👇")

    @loader.owner
    async def leftcmd(self, message):
        """Replies with 👈"""
        await message.edit("👈")

    @loader.owner
    async def rightcmd(self, message):
        """Replies with 👉"""
        await message.edit("👉")

    @loader.owner
    async def likecmd(self, message):
        """Replies with 👍"""
        await message.edit("👍")

    @loader.owner
    async def emojicmd(self, message):
        """Replies with random emoji"""
        emojilist = """😀 😃 😄 😁 😆 😅 😂 🤣 ☺️ 😊 😇 🙂 🙃 😉 😌 😍 🥰 😘 😗
 😙 😚 😋 😛 😝 😜 🤪 🤨 🧐 🤓 😎 🤩 🥳 😏 😒 😞 😔 😟 😕 🙁 ☹️ 😣 😖 😫 😩 🥺 😢
 😭 😤 😠 😡 🤬 🤯 😳 🥵 🥶 😱 😨 😰 😥 😓 🤗 🤔 🤭 🤫 🤥 😶 😐 😑 😬 🙄 😯 😦
 😧 😮 😲 🥱 😴 🤤 😪 😵 🤐 🥴 🤢 🤮 🤧 😷 🤒 🤕 🤑 🤠"""
        await message.edit(random.choice(list(emojilist.split(" "))))

    @loader.owner
    async def loopcmd(self, message,):
        """Sends the replied to message 50 times"""
        reply_message = await message.get_reply_message()
        chat = message.chat_id
        for x in range(50):
            await message.client.send_message(chat, reply_message)
