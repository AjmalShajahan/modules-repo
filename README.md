# friendly-telegram-modules-repo

## About

This is a mixed fork of modules made especially for [Friendly-Telegram](https://gitlab.com/friendly-telegram/friendly-telegram) userbot.

If you want unofficial modules from other userbots that works on Friendly-Telegram  look at the [FTG UserBot Modules](https://t.me/FTGModules) channel.

## Installation

Add this link:
```
 'https://gitlab.com/AjmalShajahan/friendly-telegram-modules/-/raw/master'
```
to MODULES_REPO variable in configuration

## Use at own risk
